//
// Created by Mykhel Frankevych on 24.05.17.
//

#ifndef PISCINE_CPP_OBJECT_HPP
#define PISCINE_CPP_OBJECT_HPP

#include <map>

class Object {
public:
	Object() { return ; };
	Object( float _x, float _y, int _speed, char _symbol, int _color ) :
			_symbol(_symbol), _color(_color), _speed(_speed), _x(_x), _y(_y) {
		_y_speed = 0;
		_x_speed = 1;
		return ;
	}

	Object(Object const & object);
	void operator=( Object const & rhs );

	virtual ~Object() { return ; };

	void moveX(float x) { _x += x ; }
	void moveY(float y) { _y += y ; }

	char get_symbol() const { return _symbol; }
	int get_color() const { return _color; }
	float get_speed() const { return _speed; }
	float get_x() const { return _x; }
	float get_y() const { return _y; }
	float get_x_speed() const { return _x_speed; }
	float get_y_speed() const { return _y_speed; }

	void set_x_vector ( float x );
	void set_y_vector ( float y );

private:
	char					_symbol;
	int						_color;
	float 					_speed;
	float 					_x_speed;
	float 					_y_speed;
	float 					_x;
	float 					_y;
};


#endif //PISCINE_CPP_OBJECT_HPP
