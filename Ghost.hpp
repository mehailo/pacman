//
// Created by Mykhel Frankevych on 24.05.17.
//

#ifndef PISCINE_CPP_GHOST_HPP
#define PISCINE_CPP_GHOST_HPP

#include "Object.hpp"

class Ghost : public Object {
public:
	Ghost() { return ; }
	Ghost(int x, int y) : Object(x, y, 1, '&', (x + y) % 2 + 1) { return ; }

	~Ghost() { return ; }
};

#endif //PISCINE_CPP_GHOST_HPP
