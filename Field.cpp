//
// Created by Mykhel Frankevych on 25.05.17.
//

#include "Field.hpp"
#include <ncurses.h>
#define SPEED_KOEF 100

Field::Field( std::vector<std::string> _map ) : _map( _map ), _score(0) {
	for ( int i = 0; i < _map.size(); i++ ) {
		for ( int j = 0; j < _map[i].length(); j++ ) {
			if ( _map[i][j] == 'g' ) {
				_ghosts.push_back(Ghost( j, i ));
				_random_ghost_vector(_ghosts.back());
				this->_map[i][j] = '.';
			}
			else if ( _map[i][j] == 'p' ) {
				_pacman = Pacman( j, i );
				this->_map[i][j] = '.';
			}
		}
	}
	srand(time(nullptr));

	return ;
}


void Field::view() {
	_view_static_map();
	for ( int i = 0; i < _ghosts.size(); i++ ) {
		attron(COLOR_PAIR(2));
		mvprintw(static_cast<int>(_ghosts[i].get_y()), static_cast<int>(_ghosts[i].get_x()),
															 "%c", _ghosts[i].get_symbol());
		attroff(COLOR_PAIR(2));
	}
	attron(COLOR_PAIR(1));
	mvprintw(static_cast<int>(_pacman.get_y()), static_cast<int>(_pacman.get_x()),
													 "%c", _pacman.get_symbol());
	attron(COLOR_PAIR(1));

	return ;
}

void Field::static_move() {
	for ( int i = 0; i < _ghosts.size(); i++ ) {
		if (_map[static_cast<int>(_ghosts[i].get_y()) + _ghosts[i].get_y_speed() / SPEED_KOEF]
			[static_cast<int>(_ghosts[i].get_x()) + _ghosts[i].get_x_speed() / SPEED_KOEF / 2] == '0' ||
				_ghosts[i].get_y() >= _map.size() || _ghosts[i].get_x() > _map[0].length())
			_random_ghost_vector(_ghosts[i]);
		_ghosts[i].moveX(_ghosts[i].get_x_speed() / SPEED_KOEF);
		_ghosts[i].moveY(_ghosts[i].get_y_speed() / SPEED_KOEF);
	}
	if (_map[_pacman.get_y() + _pacman.get_y_speed() / SPEED_KOEF]
			[_pacman.get_x() + _pacman.get_x_speed() / SPEED_KOEF] == '0' ||
			_pacman.get_y() >= _map.size() || _pacman.get_x() > _map[0].length())
		stop();
	_pacman.moveX(_pacman.get_x_speed() / SPEED_KOEF);
	_pacman.moveY(_pacman.get_y_speed() / SPEED_KOEF);
	_check_dot();
	_check_enemy();
	_check_finish();

	return ;
}

void Field::_view_static_map() {
	init_pair(3, COLOR_GREEN, 0);

	for ( int i = 0; i < _map.size(); i++ ) {
		for ( int j = 0; j < _map[i].length(); j++ ) {
			if (_map[i][j] == '0') {
				attron(COLOR_PAIR(5));
				mvprintw(i, j, " ");
				attroff(COLOR_PAIR(5));
			}
			else if (_map[i][j] == '.') {
				attron(COLOR_PAIR(2));
				mvprintw(i, j, ".");
				attroff(COLOR_PAIR(2));
			}
			else if (_map[i][j] == ' ') {
				mvprintw(i, j, " ");
			}
		}
	}
	attron(COLOR_PAIR(2));
	mvprintw(_map.size(), 15, "Score: %06d", _score);
	attroff(COLOR_PAIR(2));

	return ;
}

void Field::move_up() {
	_pacman.set_y_vector(-1);

	return ;
}

void Field::move_down() {
	_pacman.set_y_vector(1);

	return ;
}

void Field::move_left() {
	_pacman.set_x_vector(-2);

	return ;
}

void Field::move_right() {
	_pacman.set_x_vector(2);

	return ;
}

void Field::stop() {
	_pacman.set_x_vector(0);
}

void Field::_check_dot() {
	if (_map[_pacman.get_y()][_pacman.get_x()] == '.')
	{
		_map[_pacman.get_y()][_pacman.get_x()] = ' ';
		_score += 100;
	}

	return ;
}

void Field::_check_enemy() {
	for (int i = 0; i < _ghosts.size(); i++) {
		if (static_cast<int>(_ghosts[i].get_x()) == static_cast<int>(_pacman.get_x()) &&
			static_cast<int>(_ghosts[i].get_y()) == static_cast<int>(_pacman.get_y()))
			_pacman.set_hp(_pacman.get_hp() - 1);
	}
}

void Field::_random_ghost_vector(Ghost &ghost) {
	int i;

	do {
		i = _random(0, 3);

		switch (i) {
			case 0:
				ghost.set_y_vector(1);
				break ;
			case 1:
				ghost.set_y_vector(-1);
				break ;
			case 2:
				ghost.set_x_vector(-2);
				break ;
			case 3:
				ghost.set_x_vector(2);
				break ;
			default:
				break ;
		}
	} while (_map[static_cast<int>(ghost.get_y()) + ghost.get_y_speed() / SPEED_KOEF]
				[static_cast<int>(ghost.get_x()) + ghost.get_x_speed() / SPEED_KOEF / 2] == '0');
}

void Field::_check_finish() {
	if (_map[_pacman.get_y()][_pacman.get_x()] == 'f') {
		_score += 4200;
		_pacman.set_hp(0);
	}
}

int Field::_random(int floor, int ceiling) {
	int randNum = rand() % (ceiling - floor + 1) + floor;

	return (randNum);
}






