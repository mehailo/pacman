//
// Created by Mykhel Frankevych on 24.05..7.

#include "Field.hpp"


void init_ncurses(WINDOW **screen) {
	initscr();
	clear();
	noecho();
	cbreak();
	keypad(*screen, TRUE);
	start_color();
	init_pair(1, COLOR_GREEN, 0);
	init_pair(2, COLOR_BLUE, 0);
	init_pair(3, 0, COLOR_WHITE);
	init_pair(4, 0, COLOR_WHITE);
	init_pair(5, 0, COLOR_BLUE);

	return;
};

void print_start_screen() {
	const char *raw_logo[] = {"      ___          ___          ___          ___          ___          ___     \n",
											 "     /\\  \\        /\\  \\        /\\  \\        /\\__\\        /\\  \\        /\\__\\    \n",
											 "    /::\\  \\      /::\\  \\      /::\\  \\      /::|  |      /::\\  \\      /::|  |   \n",
											 "   /:/\\:\\  \\    /:/\\:\\  \\    /:/\\:\\  \\    /:|:|  |     /:/\\:\\  \\    /:|:|  |   \n",
											 "  /::\\~\\:\\  \\  /::\\~\\:\\  \\  /:/  \\:\\  \\  /:/|:|__|__  /::\\~\\:\\  \\  /:/|:|  |__ \n",
											 " /:/\\:\\ \\:\\__\\/:/\\:\\ \\:\\__\\/:/__/ \\:\\__\\/:/ |::::\\__\\/:/\\:\\ \\:\\__\\/:/ |:| /\\__\\\n",
											 " \\/__\\:\\/:/  /\\/__\\:\\/:/  /\\:\\  \\  \\/__/\\/__/~~/:/  /\\/__\\:\\/:/  /\\/__|:|/:/  /\n",
											 "      \\::/  /      \\::/  /  \\:\\  \\            /:/  /      \\::/  /     |:/:/  / \n",
											 "       \\/__/       /:/  /    \\:\\  \\          /:/  /       /:/  /      |::/  /  \n",
											 "                  /:/  /      \\:\\__\\        /:/  /       /:/  /       /:/  /   \n",
											 "                  \\/__/        \\/__/        \\/__/        \\/__/        \\/__/    "};


	attron(COLOR_PAIR(2));
	for ( int i = 0; i < 11; i++ ) {
		for ( int j = 0; j < 80; j++ ) {
			mvprintw(i, j, "%c", raw_logo[i][j]);
		}
	}
	attroff(COLOR_PAIR(2));

	attron(COLOR_PAIR(2));
	mvprintw(11, 30, "%s", "Press ENTER to start");
	mvprintw(12, 10, "%s", "Control with arrow keys, space for pause, esc for exit");
	mvprintw(13, 20, "%s", "You need to get to the bottom hole, gl hf");
	attroff(COLOR_PAIR(2));

}

void init_screen(WINDOW *screen)
{
	int key;
	int exit_flag = 0;

	while (true)
	{
		print_start_screen();
		refresh();
		key = wgetch(screen);
		switch (key) {
			case '\n':
				exit_flag = 1;
				break;
			case 27:
				exit(0);
		}
		if (exit_flag)
			break;
		print_start_screen();
		refresh();
	}
}

int main( void ) {
	std::ifstream 				input( "map" );
	std::vector<std::string>	map;
	WINDOW 						*screen;
	int 						key;
	int 						exit_flag = 0;
	int							frame = 0;

	//----------------ncurses stuff--------------------//
	init_ncurses(&screen);

	//----------------reading file--------------------//
	for( std::string line; getline( input, line ); ) {
		map.push_back( line );
	}

	//----------------init screen--------------------//
	screen = newwin(map.size(), map[0].length(), 0, 0);
	Field	game(map);


	wtimeout(screen, 1);
	keypad(screen, TRUE);
	init_screen(screen);
	game.get_pacman().set_y_vector(-1);
	while (true)
	{
		key = wgetch(screen);
		switch (key)
		{
			case KEY_UP:
				game.move_up();
				break;
			case KEY_DOWN:
				game.move_down();
				break;
			case KEY_LEFT:
				game.move_left();
				break;
			case KEY_RIGHT:
				game.move_right();
				break;
			case ' ':
				while (true)
				{
					key = wgetch(screen);
					if (key == ' ')
						break ;
				}
				break;
			case 27:
				exit_flag = 1;
				break;
			default:
				break;
		}
		game.static_move();
		game.view();
		refresh();
		if (game.get_pacman().get_hp() <= 0)
		{
			for(;;)
			{
				attron(COLOR_PAIR(4));
				mvprintw(8, 32, "GAME OVER");
				mvprintw(9, 30, "SCORE: %06d", game.get_score());
				refresh();
				key = wgetch(screen);
				if (key == 27)
					return 0;
			}
		}
		if (exit_flag)
			break;
		frame++;
	}
	clrtoeol();
	endwin();
	return 0;
}

//std::vector<std::string> map = {"00000000000000000000",
//								"0..................0",
//								"0.0000000.00000000.0",
//								"0.0000000.00000000.0",
//								"....g...............",
//								"0.0000000.00000000.0",
//								"0.0000000.00000000.0",
//								"0.0000000.00000000.0",
//								"0.0000000.00000000.0",
//								"....................",
//								"00000.000p0000.00000",
//								"....................",
//								"0.000.000.000.0000.0",
//								"0.000.000.000.0000.0",
//								"0g000.........0000.0",
//								"0.0000000.00000000.0",
//								".............g......",
//								"0.0000000.00000000.0",
//								"0.0000000.00000000.0",
//								"0..................0",
//								"00000000000000000000"};
