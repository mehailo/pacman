NAME = pacman

SRC = main.cpp \
      Object.cpp \
      Field.cpp \

OBJ = $(SRC:.cpp=.o)

HEADER = Object.hpp \
         Ghost.hpp \
         Pacman.hpp \
         Field.hpp

CC = clang++

CFLAGS = -c -Wall -Werror -Wextra

OFLAGS = -lncurses

all: $(NAME)

$(NAME): $(OBJ)
		$(CC) -o $(NAME) $(OBJ) $(OFLAGS)

%.o: %.c $(HEADER)
		$(CC) $(CFLAGS) -o $@ $<

clean:
		rm -f $(OBJ)

fclean: clean
		rm -f $(NAME)

re: fclean all
