//
// Created by Mykhel Frankevych on 24.05.17.
//

#include "Object.hpp"


Object::Object( Object const &object ) {
	*this = object;

	return ;
}

void Object::operator=( Object const &rhs ) {
	_symbol = rhs.get_symbol();
	_color = rhs.get_color();
	_speed = rhs.get_speed();
	_x = rhs.get_x();
	_y = rhs.get_y();
	_x_speed = rhs.get_x_speed();
	_y_speed = rhs.get_y_speed();

	return ;
}

void Object::set_x_vector( float x ) {
	_y_speed = 0;
	_x_speed = x;

	return ;
}

void Object::set_y_vector( float y ) {
	_y_speed = y;
	_x_speed = 0;

	return ;
}


