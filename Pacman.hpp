//
// Created by Mykhel Frankevych on 24.05.17.
//

#ifndef PISCINE_CPP_PACMAN_HPP
#define PISCINE_CPP_PACMAN_HPP

#include "Object.hpp"

class Pacman : public Object {
public:
	Pacman() { return ; }
	Pacman( int x, int y ) : Object(x, y, 2, 'O', 1) {
		_hp = 1;
		return ;
	}

	~Pacman() { return ; }

	int get_hp() const { return _hp; }
	void set_hp(int _hp) { Pacman::_hp = _hp; }

private:
	int _hp;
};

#endif //PISCINE_CPP_PACMAN_HPP
