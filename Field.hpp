//
// Created by Mykhel Frankevych on 25.05.17.
//

#ifndef PISCINE_CPP_FIELD_HPP
#define PISCINE_CPP_FIELD_HPP

#include <vector>
#include <string>
#include <iostream>
#include <ctime>
#include <curses.h>
#include <vector>
#include <fstream>

#include "Ghost.hpp"
#include "Pacman.hpp"

class Field {
public:
	Field() { return ; }
	Field( std::vector<std::string> _map );

	virtual ~Field() { return ; }

	Pacman &get_pacman() { return _pacman; }
	std::vector<Ghost> &get_ghosts() { return _ghosts; }
	const std::vector<std::string> &get_map() const { return _map; }
	int get_score() const { return _score; }

	void move_up();
	void move_down();
	void move_left();
	void move_right();
	void stop();

	void view();
	void static_move();

private:
	Pacman						_pacman;
	std::vector<Ghost>			_ghosts;
	std::vector<std::string>	_map;
	int 						_score;

	void _view_static_map();
	void _check_dot();
	void _check_enemy();
	void _check_finish();
	void _random_ghost_vector(Ghost & ghost);

	int _random(int floor, int ceiling);
};


#endif //PISCINE_CPP_FIELD_HPP
